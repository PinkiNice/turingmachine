import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import turingMachineApp from './reducers/turingMachineApp'
import App from './components/App'
import { initState } from './initState'

let state = initState();

let store = createStore(turingMachineApp, state)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)