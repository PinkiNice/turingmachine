//Represent buttons near the Tape
import React from 'react'
import { connect } from 'react-redux'
import { moveTape } from '../actions'


const onButtonMouseOver = (e) => {
  e.target.style = 'background-color: #E0E0E0;'
}

const onButtonMouserOut = (e, chosen) => {
  e.target.style = `background-color: #F3F1F1`
}

let MoveTape = ({ dispatch, side }) => {
  return (
    <div 
      className='moveTapeButton inline'
      onClick={e => {
        dispatch(moveTape(side));
      }}
      onMouseOver={(e) => onButtonMouseOver(e)}
      onMouseOut={(e) => onButtonMouserOut(e)}
    >
      {side == -1 ? '<-' : '->'}
    </div>
  )
}

MoveTape = connect()(MoveTape);

export default MoveTape;