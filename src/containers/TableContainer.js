import React from 'react'
import { connect } from 'react-redux'
import Table from '../components/Table'
import { onTextAreaChange } from '../actions'

const mapStateToProps = (state) => {
  return {
    haveNoIdeaWhyINeedThisButItWorksThisWay: state,
    states: state.table,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTextAreaChange: (id, actIndex, newValue) => dispatch(onTextAreaChange(id, actIndex, newValue))
  }
};

const TableCont = connect(
  mapStateToProps,
  mapDispatchToProps
)(Table)

export default TableCont