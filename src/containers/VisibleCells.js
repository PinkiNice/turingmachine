/**
 *  This represents the tape itself.
 *  Inlcudes container + component
 */

import React, { PropTypes } from 'react'
import Cells from '../components/Cells'
import { connect } from 'react-redux'
import { onCellClick } from '../actions'

const getVisibleCells = (cells, index, tapeLength) => {
  const i = cells.indexOf(cells.find(cell => cell.id == index));
  const slic = cells.slice(i, i + tapeLength);
  return slic;
}

const mapStateToProps = (state) => {
  return {
    cells: getVisibleCells(state.cells, state.leftIndex, state.tapeLength),
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    onCellClick: id => dispatch(onCellClick(id))
  }
}

const VisibleCells = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cells)

export default VisibleCells