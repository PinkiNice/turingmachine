import React from 'react'
import { connect } from 'react-redux'
import StateDisplay from '../components/StateDisplay'

const mapStateToProps = (state) => {
  return {
    display: state.currentStateId == 'S' ? 'qS' : 'q' + state.currentStateId
  }
}

//
const StateDisplayCont = connect(
  mapStateToProps
)(StateDisplay);

export default StateDisplayCont