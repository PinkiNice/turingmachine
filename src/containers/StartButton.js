import React from 'react'
import { connect } from 'react-redux'
import { toggleMachine } from '../actions'

let StartButton = ({ dispatch, active, label }) => (
  <div onClick={e => { active ? dispatch(toggleMachine()) : 'lol'}} style={{backgroundColor: active ? 'white' : 'gray'}}>
    {label}
  </div>
)

const checkValidity = (table) => {
  return !table.filter(state => !state.acts[0].valid || !state.acts[1].valid).length
};

const mapStateToProps = (state) => {
  return {
    label: state.isRunning ? 'stop' : 'start',
    active: checkValidity(state.table)
  }
}

StartButton = connect(
  mapStateToProps,
)(StartButton);

export default StartButton