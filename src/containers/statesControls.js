import React from 'react'
import { connect } from 'react-redux'

const onButtonMouseOver = (e) => {
  e.target.style = 'background-color: #E0E0E0;'
}

const onButtonMouserOut = (e, chosen) => {
  e.target.style = `background-color: #F3F1F1`
}

let StateControlButton = ({ dispatch, action, label }) => (
  <div className='state-control'
    onClick={e => dispatch(action)}
    onMouseOver={(e) => onButtonMouseOver(e)}
    onMouseOut={(e) => onButtonMouserOut(e)}
  >
    {label}
  </div> 
)

StateControlButton = connect()(StateControlButton)

export default StateControlButton
