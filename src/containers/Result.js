import React from 'react'
import { connect } from 'react-redux'
import { getResult } from '../actions'

let Result = ({ result }) => (
  <div className='result'>
      <ul>
      {result.map((oneStepInfo) => <li>{oneStepInfo}</li>)}
      </ul>
  </div>
)

const mapStateToProps = (state) => {
  return {
    result: formResult(state.result)
  }
}

const formResult = (result) => {
  return result.map(oneStepInfo => oneStepInfo.join(' '))
}

Result = connect(
  mapStateToProps,
)(Result);

export default Result