import React from 'react'
import { connect } from 'react-redux'
import StateDisplay from '../components/StateDisplay'

const mapStateToProps = (state) => {
  return {
    display: state.step
  }
}

const StepDisplayCont = connect(
  mapStateToProps
)(StateDisplay);

export default StepDisplayCont