// Constructors for actions

export const moveTape = (side) => {
  return {
    type: 'MOVE_TAPE',
    side
  }
}

export const onCellClick = (id) => {
  return {
    type: 'CELL_CLICK',
    id
  }
}

export const getResult = () => {
  return {
    type: 'GET_RESULT'
  }
}

export const onTextAreaChange = (stateId, actId, newValue) => {
  return {
    type: 'TEXT_AREA_CHANGE',
    stateId,
    actId,
    newValue
  }
}

export const toggleMachine = () => {
  return {
    type: 'TOGGLE_MACHINE'
  }
}

export const addState = () => {
  return {
    type: 'ADD_STATE'
  }
}

export const removeState = () => {
  return {
    type: 'REMOVE_STATE'
  }
}

export const stepAction = () => {
  return {
    type: 'DO_STEP'
  }
}

export const resetAction = () => {
  return {
    type: 'RESET'
  }
}

export const setState = (id) => {
  return {
    type: 'SET_STATE',
    id
  }
}