import React from 'react'

const StateDisplay = ({label, display}) => (
  <div className='state-display'>
    {label ? label + ':' : ''} {display}
  </div>
)

export default StateDisplay;