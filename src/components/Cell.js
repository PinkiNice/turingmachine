import React, { PropTypes } from 'react'


// This two are responsible for changing color of cells when mouse is on.
const onCellMouseOver = (e) => {
  e.target.style = 'background-color: #1297BD;'
}
const onCellMouseOut = (e, chosen) => {
  e.target.style = `background-color: ${chosen ? '#4C74C9': '#1AD7DB'}`
}

// chosen - Bool if the Tape's head is currently over this Cell
const Cell = ({ chosen, value, onMouseOver, onClick }) => (
  <div 
    className='cell inline'
    style={{
      'backgroundColor': chosen ? '#4C74C9': '#1AD7DB'
    }}
    onMouseOver={(e) => onCellMouseOver(e)}
    onMouseOut={(e) => onCellMouseOut(e, chosen)}
    onClick={onClick}
  >
    { value }
  </div>
);

Cell.PropTypes = {
  onClick: PropTypes.func.isRequired,
  onMouserOver: PropTypes.func.isRequired,
  chosen: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired
}

export default Cell