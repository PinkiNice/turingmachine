import React, { PropTypes } from 'react'
import TableRow from './TableRow'

const Table = ({ states, onTextAreaChange }) => (
  <div className='statesDiv'>
    <table className='statesTable'>
      <tbody>
        <tr>
          <th> State </th>
          <th> S0 </th>
          <th> 0 </th>
          <th> 1 </th>
          <th> 2 </th>
        </tr>
        {states.map(state => 
          <TableRow 
            state={state}
            key={state.id}
            onTextAreaChange={onTextAreaChange} />
        )}
      </tbody>
    </table>
  </div>
)

Table.PropTypes =  {
  states: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    act: PropTypes.arrayOf(PropTypes.shape({
      nextState: PropTypes.number.isRequired,
      write: PropTypes.number.isRequired,
      move: PropTypes.number.isRequired
    })).isRequired
  }))
}

export default Table
// N - no moves, L - left move, R - right move.
// qX, Y, Z 
// Where X - number of the next state, Y - what to write, Z - how to move
/*    0               1
q0  q1, 1, L        q1, 0, R
q1  q0, 1, L          ...  
q2  q4, 0, R          ...
q3    ...          S, 1, N
q4    ...             ...
q5    ...             ...
q6    ...             ...
 <- There should be a buttons '+' and '-' which can add states and remove them
 */
/*
arrayOfStates =[
  {
    id: 0,
    zero: { nextState: 1, write: 1, move: -1 },
    one: { nextState: 1, write: 0, move: 1 }
  }, {
    id: 1,
    zero: { nextState: 0, write: 1, move: -1 },
    one: {}
  }, {
  ...
  }, {
    id: 3,
    zero: { ... },
    one: { nextState: 'S', write: 1, move: 0 }
  }
]

*/

// S = Stop the machine.
// If 'nextState' of executed state is 'S' - we stop the machine
// If cell is empty - we will count this as a command to Stop