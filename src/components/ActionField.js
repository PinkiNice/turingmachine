 import React from 'react'
 
 const ActionField = ({ stateId, actObj, onTextAreaChange }) => (
  <textarea
    onChange={e => onTextAreaChange(stateId, actId, e.target.value)}
    value={actObj.string}
    style={{ backgroundColor: actObj.valid ? 'white' : 'red' }}
  />
);

export default ActionField