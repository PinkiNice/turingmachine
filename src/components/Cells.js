import React, { PropTypes } from 'react'
import Cell from './Cell'

const Cells = ({ cells, onCellClick, onCellMouseOver }) => (
  <div className="Cells inline">
    {cells.map(cell => 
      <Cell 
        key={cell.id}
        { ...cell }
        onClick={() => onCellClick(cell.id)}
        onMouseOver={() => onCellMouseOver(cell.id)}
      />
    )}
  </div>
)

Cells.PropTypes = {
  cells: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    chosen: PropTypes.bool.isRequired,
    value: PropTypes.string.isRequired
  }).isRequired).isRequired
}

export default Cells