import React from 'react'

//containers
import MoveTape from '../containers/MoveTape'
import VisibleCells from '../containers/VisibleCells'
import TableCont from '../containers/TableContainer'
import StateControlButton from '../containers/statesControls'
import StateDisplayCont from '../containers/StateDisplayCont'
import StepDisplayCont from '../containers/StepDisplayContainer'
import Result from '../containers/Result'
import ResultDisplayCont from '../containers/ResultDisplayContainter'
import { getResult, setState, addState, removeState, stepAction, resetAction } from '../actions'

const App = () => (
  <div>
    <div className='container tape'>
      <MoveTape side='-1' />
      <VisibleCells/>
      <MoveTape side='1' />
    </div>
    <TableCont />
    <div className='state-controls'>
      <StateControlButton action={addState()} label='+' />
      <StateControlButton action={removeState()} label='-' />
      <div className='machine-controls'>
        <StateControlButton action={stepAction()} label='step' />
        <StateControlButton action={resetAction()} label='reset' />
        <StateControlButton action={getResult()} label='result' />
      </div>
      <div className='tests'>
        <StateControlButton action={setState(1)} label='test1' />
        <StateControlButton action={setState(2)} label='test2' />
        <StateControlButton action={setState(3)} label='test3' />
        <StateControlButton action={setState(4)} label='test4' />
      </div>
    </div>
    <div className='displays'>
      <StateDisplayCont label='State'/>
      <StepDisplayCont label='Step' />
    </div>
    <Result />
  </div>
)

export default App