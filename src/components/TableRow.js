import React, { PropTypes } from 'react'

//      <ActionField id={id} act={zero} />
const TableRow = ({ state, onTextAreaChange }) => (
  <tr>
    <td> 
      {'q'+ state.id} 
    </td>
    <td> 
      <textarea 
        value={state.acts[0] ? state.acts[0].string: 'S'}
        style={{ backgroundColor: state.acts[0].valid ? 'white' : 'red' }}
        onChange={e => onTextAreaChange(state.id, 0, e.target.value)}
      />
    </td>
    <td> 
      <textarea
        value={state.acts[1] ? state.acts[1].string: 'S'}
        style={{ backgroundColor: state.acts[1].valid ? 'white' : 'red' }}
        onChange={e => onTextAreaChange(state.id, 1, e.target.value)}
      />
    </td>
    <td> 
      <textarea
        value={state.acts[2] ? state.acts[2].string: 'S'}
        style={{ backgroundColor: state.acts[2].valid ? 'white' : 'red' }}
        onChange={e => onTextAreaChange(state.id, 2, e.target.value)}
      />
    </td>
    <td> 
      <textarea
        value={state.acts[3] ? state.acts[3].string: 'S'}
        style={{ backgroundColor: state.acts[3].valid ? 'white' : 'red' }}
        onChange={e => onTextAreaChange(state.id, 3, e.target.value)}
      />
    </td>
  </tr>
);

export default TableRow