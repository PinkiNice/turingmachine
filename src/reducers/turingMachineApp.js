import { initState } from '../initState'

const reset = (state = [], action) => {
  if (action.type === 'RESET') return initState();
  return state;
}

const cell = (state = [], action) => {
  const arr = ['S0', 0, 1, 2];
  switch (action.type){
    case 'CELL_CLICK':
      const newState = Object.assign({}, state);
      const clickedCell = newState.cells.find(cell => cell.id == action.id);
      clickedCell.value = arr[(arr.findIndex(item => item == clickedCell.value) + 1) % 4]
      return newState;
    default:
      return state;
  }
}

const tape = (state = [], action) => {
  switch (action.type) {
    case 'MOVE_TAPE':
      const newState = Object.assign({}, state);

      if (action.side == -1) {
        newState.leftIndex = parseInt(newState.leftIndex) - 1;
         if (newState.cells.filter(cell => parseInt(cell.id) === newState.leftIndex).length == 0) {
          newState.cells.unshift({
            id: newState.leftIndex,
            chosen: false,
            value: 'S0'
          })
        }
      } else if (action.side == 1) {
        newState.leftIndex = parseInt(newState.leftIndex) + 1;
        const rightCellIndex = newState.leftIndex + parseInt(newState.tapeLength) -1;
        if (newState.cells.filter(cell => parseInt(cell.id) === rightCellIndex).length == 0) {
          newState.cells.push({
            id: rightCellIndex,
            chosen: false,
            value: 'S0'
          })
        }
      }
      return newState;
    default:
      return state
    }
}

const onTextAreaChange = (state = [], action) => {
  const { stateId, actId, newValue } = action;
  const newState = Object.assign({}, state);
  newState.table[stateId].acts[actId].string = newValue;
  const res = validateActionString(newValue);
  const act = newState.table[stateId].acts[actId];

  if (res) {
    newState.table[stateId].acts[actId] = res;
  } else {
    newState.table[stateId].acts[actId].valid = false;
  }

  return newState;
}

function validateActionString(str) {
  // q[1-9] [0-1] [N, L, R]
  const reg = /^q([0-9]+|S) ([0-3]) ([L, N, R])$/
  const result = str.match(reg)
  const transf = {
    L: -1,
    N: 0,
    R: 1
  }

  if (result) {
    return {
      string: str,
      valid: true,
      nextState: result[1],
      write: parseInt(result[2]),
      move: transf[result[3]]
    };
  }
  return false;
}

// Creates new state(It's just a constructor)
function makeMachineState(id) {
  const state = {
    id: id,
    acts: {
      0:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      1:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    },
      2:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      3:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    }}
  }
  return state;
}

const toggleMachine = (state = [], action) => {
  const newState = Object.assign({}, state);
  newState.isRunning = !newState.isRunning;
  return newState;
}

const addState = (state = [], action) => {
  const newState = Object.assign({}, state);
  newState.table.push(makeMachineState(newState.table.length));
  return newState;
}

const removeState = (state = [], action) => {
  const newState = Object.assign({}, state);
  if (newState.table.length == 2) {
    return newState;
  }
  newState.table.pop();
  return newState;
}


const checkValidity = (table) => {
  return !table.filter(state => !state.acts[0].valid || !state.acts[1].valid).length
};

const step = (state = [], action) => {
  if (!checkValidity(state.table)) {
    return state;
  }

  let newState = Object.assign({}, state);

  const currentStateId = newState.currentStateId; //Current state id
  if (currentStateId == 'S') {
    return newState;
  }
  
  newState.step = Number(newState.step) + 1;
  const machineState = newState.table[currentStateId]; //Current state object
  const chosenCell = newState.cells.find(cell => cell.chosen == true);   // Cell object which is chosen
  const currentTapeSymbol = chosenCell.value == 'S0' ? 0 : Number(chosenCell.value) + 1;     // What is in the cell
  const act = machineState.acts[currentTapeSymbol];
  const newSymbol = act.write;
  const move = act.move;
  const nextStateId = act.nextState
  chosenCell.value = newSymbol;
  const cellIdAfterMovement = chosenCell.id - 0 + move;
  // Move tape and create new cell if we still don't have one;
  newState = tape(newState, {type: 'MOVE_TAPE', side: move})
  //set new cell to be chosen one
  chosenCell.chosen = false;
  const newCell = newState.cells.find(cell => cell.id == cellIdAfterMovement)
  newCell.chosen = true;

  if (nextStateId == 'S') {
    newState.currentStateId = 'S';
    newState.isRunning = false;
  }
  newState.currentStateId = nextStateId;
  return newState
}

const setState = (state, action) => {
  const id = action.id;
  // set every act of every machineState to validateString.
  // and we have all states filled just from 
  switch (id) {
    case 1:
      return testState1();
    case 2: 
      return testState2();
    case 3:
      return testState3();
    case 4:
      return testState4();
    default:
      return state;
  }
}

const getResult = (state, action) => {
  /*
  cell.id <= 0 or cell.id <0 ??????77 have no idea need to rethink
   */
  function getTapeBorders(cells) {
    const leftId = cells.reduce((id, cell) => {
      return cell.value == 'S0' && cell.id <= 0 && cell.id > id ? cell.id : id;
    }, cells[0].id)
    const rightId = cells.reduce((id, cell) => {
      return cell.value == 'S0' && cell.id > 0 && cell.id < id ? cell.id : id;
    }, cells[cells.length - 1].id)
    console.log(leftId, rightId);
    return [leftId, rightId];
  }

  // Create deep copy of a cell
  const copyCell = (cell) => {
    return {
      id: cell.id,
      value: cell.value,
      chosen: cell.chosen
    }
  }
  // If action somehow is not fit
  if (action.type != 'GET_RESULT') return state;
  // DOES NOT DEEP COPY EVERYTHING(objects are the same)
  const newState = Object.assign({}, state);
  let sandboxState = Object.assign({}, state); 
  //deep copy of all cells
  const cells = state.cells.map(cell => copyCell(cell)); 
  // Replace all cells in state with copies
  sandboxState.cells = cells;
  // After steps we made before we can work with sandboxState just as with copy unless we don't
  // change it's objects.
  
  let currentStep = 0;
  let result = [];
  while (currentStep <= 100 && sandboxState.currentStateId !== 'S') {
    sandboxState = step(sandboxState);
    const [leftBorder, rightBorder] = getTapeBorders(sandboxState.cells);
    let stepInfo = [
      ...sandboxState.cells
        .filter(cell => cell.id >= leftBorder && cell.id <= rightBorder)
        .map(cell => cell.chosen ? '*' + cell.value : cell.value),
      `Step: ${sandboxState.step}`
    ]
    result.push(stepInfo)
    currentStep++;
  }

  
  if (sandboxState.currentStateId !== 'S') {
    result = [['Your program tooks too many steps to be completed. Are you sure it\'s not infinite?']];
  }

  newState.result = result;
  return newState;
}

const turingMachineApp = (state = [], action) => {
  console.log(action.type);
  switch (action.type) {
    case 'MOVE_TAPE':
      return tape(state, action);
    case 'CELL_CLICK':
      return cell(state, action);
    case 'TEXT_AREA_CHANGE':
      return onTextAreaChange(state, action);
    case 'TOGGLE_MACHINE':
      return toggleMachine(state, action);
    case 'ADD_STATE':
      return addState(state, action);
    case 'REMOVE_STATE':
      return removeState(state, action);
    case 'DO_STEP':
      return step(state, action);
    case 'RESET':
      return reset(state, action);
    case 'SET_STATE':
      return setState(state, action);
    case 'GET_RESULT':
      return getResult(state, action);
    default:
      return state;
  }
}

const testState = (q0, q1, q2, cells) => {
  const state = initState();
  const state0 = makeMachineState(0);
  const state1 = makeMachineState(1);
  const state2 = makeMachineState(2);
  for (let i = 0; i <= 3; ++i) {
    state0.acts[i] = validateActionString(q0[i]);
    state1.acts[i] = validateActionString(q1[i]);
    state2.acts[i] = validateActionString(q2[i]);
  }

  state.cells = cells
  state.table[0] = state0;
  state.table[1] = state1;
  state.table[2] = state2;
  return state;
}

const testState1 = () => {
  const q0 = ['qS 1 L', 'q2 1 R', 'q0 2 R', 'q2 0 R'];
  const q1 = ['qS 2 N', 'q1 2 R', 'q0 0 R', 'q1 1 R'];
  const q2 = ['qS 0 R', 'q1 2 L', 'q2 2 N', 'q0 2 L'];
  const cells = [
  {id: -6, chosen: false, value: 'S0'},
  {id: -5, chosen: true, value: '0'},
  {id: -4, chosen: false, value: '0'},
  {id: -3, chosen: false, value: '1'},
  {id: -2, chosen: false, value: '1'},
  {id: -1, chosen: false, value: '2'},
  {id: 0, chosen: false, value: '2'},
  {id: 1, chosen: false, value: '1'},
  {id: 2, chosen: false, value: '1'},
  {id: 3, chosen: false, value: '0'},
  {id: 4, chosen: false, value: 'S0'},
  {id: 5, chosen: false, value: 'S0'},
  {id: 6, chosen: false, value: 'S0'}
  ]
  return testState(q0, q1, q2, cells);
}

const testState2 = () => {
  const q0 = ['qS 1 L', 'q2 1 R', 'q0 2 R', 'q2 0 R'];
  const q1 = ['qS 2 N', 'q1 2 R', 'q0 0 R', 'q1 1 R'];
  const q2 = ['qS 0 R', 'q1 2 L', 'q2 2 N', 'q0 2 L'];
  const cells = [
  {id: -6, chosen: false, value: 'S0'},
  {id: -5, chosen: true, value: '0'},
  {id: -4, chosen: false, value: '1'},
  {id: -3, chosen: false, value: '2'},
  {id: -2, chosen: false, value: '0'},
  {id: -1, chosen: false, value: '1'},
  {id: 0, chosen: false, value: '2'},
  {id: 1, chosen: false, value: '0'},
  {id: 2, chosen: false, value: '1'},
  {id: 3, chosen: false, value: '2'},
  {id: 4, chosen: false, value: 'S0'},
  {id: 5, chosen: false, value: 'S0'},
  {id: 6, chosen: false, value: 'S0'}
  ]
  return testState(q0, q1, q2, cells);
}

const testState3 = () => {
  const q0 = ['qS 1 L', 'q2 1 R', 'q0 2 R', 'q2 0 R'];
  const q1 = ['q1 2 N', 'q1 2 R', 'q0 0 R', 'q1 1 R'];
  const q2 = ['qS 0 R', 'q1 2 L', 'q2 2 N', 'q0 2 L'];
  const cells = [
  {id: -6, chosen: false, value: 'S0'},
  {id: -5, chosen: true, value: '0'},
  {id: -4, chosen: false, value: '0'},
  {id: -3, chosen: false, value: '1'},
  {id: -2, chosen: false, value: '1'},
  {id: -1, chosen: false, value: '2'},
  {id: 0, chosen: false, value: '2'},
  {id: 1, chosen: false, value: '1'},
  {id: 2, chosen: false, value: '1'},
  {id: 3, chosen: false, value: '0'},
  {id: 4, chosen: false, value: 'S0'},
  {id: 5, chosen: false, value: 'S0'},
  {id: 6, chosen: false, value: 'S0'}
  ]
  return testState(q0, q1, q2, cells);
}

const testState4 = () => {
  const q0 = ['qS 1 L', 'q2 1 R', 'q0 2 R', 'q2 0 R'];
  const q1 = ['q1 0 L', 'q1 2 R', 'q0 0 R', 'q1 1 R'];
  const q2 = ['qS 0 R', 'q1 2 L', 'q2 2 N', 'q0 2 L'];
  const cells = [
  {id: -6, chosen: false, value: 'S0'},
  {id: -5, chosen: true, value: '0'},
  {id: -4, chosen: false, value: '0'},
  {id: -3, chosen: false, value: '1'},
  {id: -2, chosen: false, value: '1'},
  {id: -1, chosen: false, value: '2'},
  {id: 0, chosen: false, value: '2'},
  {id: 1, chosen: false, value: '1'},
  {id: 2, chosen: false, value: '1'},
  {id: 3, chosen: false, value: '0'},
  {id: 4, chosen: false, value: 'S0'},
  {id: 5, chosen: false, value: 'S0'},
  {id: 6, chosen: false, value: 'S0'}
  ]
  return testState(q0, q1, q2, cells);
}

export default turingMachineApp;