export const initState = () => ({
  isRunning: false,
  cells: [
  {id: -6, chosen: false, value: 'S0'},
  {id: -5, chosen: false, value: 'S0'},
  {id: -4, chosen: false, value: 'S0'},
  {id: -3, chosen: false, value: 'S0'},
  {id: -2, chosen: false, value: 'S0'},
  {id: -1, chosen: true, value: 'S0'},
  {id: 0, chosen: false, value: 'S0'},
  {id: 1, chosen: false, value: 'S0'},
  {id: 2, chosen: false, value: 'S0'},
  {id: 3, chosen: false, value: 'S0'},
  {id: 4, chosen: false, value: 'S0'},
  {id: 5, chosen: false, value: 'S0'},
  {id: 6, chosen: false, value: 'S0'}
  ],
  leftIndex: -6,
  step: 0,
  result: [],
  delay: 100,
  currentStateId: 0,
  table: [ {
    id: 0,
    acts: {
      0:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      1:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    },
      2:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      3:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    }}
  },
  {
    id: 1,
    acts: {
      0:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      1:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    },
      2:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      3:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    }}
  },
  {
    id: 2,
    acts: {
      0:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      1:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    },
      2:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0      
    }, 
      3:{
        string: 'q0 0 N',
        valid: true,
        nextState: 0,
        write: 0,
        move: 0
    }}
  }],
  tapeLength: 11
})